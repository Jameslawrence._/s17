//let output;

/*const array1 = ['eat', 'sleep'] ;
const array2 = new Array('pray','play');
const myList = [];
const numArray = [2, 3, 4, 5];
const stringArray = ['eat','work','pray','play'];
output = array1;



document.getElementById("console").innerHTML = output;

let places = ['boracay','guimaras','palawan','baguio','tagaytag','manila','bohol'];

for(let i=0; i < places.length; i++){
	if(i == 0){
		console.log(`1st item ${places[i]}`);
	}else{
		console.log(`places: ${places[i]}`);
	}
}

console.log(places.length);
*/

let listItems= [];
let item, deleteItem, updateItem, findItem;
let index;

function addFunc(){
	item = document.getElementById("inputToDo").value;
	if(item !== ""){
		listItems.push(item);
	}
	document.getElementById("list-item").innerHTML = listItems;
	document.getElementById("inputToDo").value = " ";
}

function deleteFunc(){
	deleteItem = document.getElementById("itemDelete").value;
	for(index = 0; index < listItems.length; index++){
		if(listItems[index] == deleteItem){
			listItems.splice(index,1);
			break;
		}
	}
	document.getElementById("list-item").innerHTML = listItems;
	document.getElementById("itemDelete").value = " ";
}

function updateFunc(){
	updateItem = document.getElementById("itemUpdate").value;
	findItem = document.getElementById("itemFind").value;
	for(index = 0; index < listItems.length; index++){
		if(listItems[index] == findItem){
			console.log(listItems);
			listItems[index] = updateItem;
			break; 
		}
	}
	document.getElementById("list-item").innerHTML = listItems;
	document.getElementById("itemUpdate").value = " ";	
	document.getElementById("itemFind").value = " ";	
}

function sortAscFunc(){
	document.getElementById("list-item").innerHTML = listItems.sort();
}
function sortDescFunc(){
	document.getElementById("list-item").innerHTML = listItems.sort().reverse();
}

let startIndex, endIndex, sliceArray,joinValue;
function sliceFunc(){
	startIndex = document.getElementById("startIndexSlice").value;
	endIndex = document.getElementById("endIndexSlice").value;
	console.log(startIndex + endIndex);
	if(endIndex == 0){
		sliceArray = listItems.slice(startIndex);	
		console.log("end index does not exist");
	}else{
		sliceArray = listItems.slice(startIndex, endIndex);
	}
	console.log(sliceArray);
	document.getElementById("sliceOutput").innerHTML = sliceArray;
}
function toStringFunc(){
	document.getElementById("sliceOutput").innerHTML = listItems.toString();
}
function joinFunc(){
	joinValue = document.getElementById('joinItem').value;
	document.getElementById("sliceOutput").innerHTML = listItems.join(joinValue);
}

let 
	indexSubmit,
	dogName = [
		'toffee',
		'starry',
		'Lloyda'
	];


function indexOfFunc(){
	indexSubmit = document.getElementById("indexSubmit").value;
	for(let z = 0; z < listItems.length; z++){
		document.getElementById("sliceOutput").innerHTML = listItems.indexOf(indexSubmit);
	}
}

function lastIndexOfFunc(){
	indexSubmit = document.getElementById("indexSubmit").value;
	console.log(a.lastIndexOf('a'));
	document.getElementById("sliceOutput").innerHTML = listItems.lastIndexOf(indexSubmit);
}

/* 
	functions:
	indexOf()
	lastIndexOf()
	find()
	forEach()
	map()
	some - true stop 
	every - false stop
	filter
*/
let excludeElement;
let showElements = []; 

function forEachFunc(){
	excludeElement = document.getElementById("itemNotInclude").value;
	listItems.forEach(function(excludeElement){
		for(let h = 0; h < listItems.length; h++){
			if(excludeElement != listItems[h]){
				document.getElementById("sliceOutput").innerHTML = listItems;		
			}
		}
	});
}

let username, listUserName;

function loginBtn(){
	username = document.getElementById("userName").value;
/*	let userFound = listItems.find(function(listUserName){
		return listUserName = username;
	});
	console.log(userFound);*/
	let doesEmailExist = listItems.includes(username);
	if(doesEmailExist == true){
		alert("Email Already Exist");
	}else{
		alert("proceed to registration");
	}
	console.log(doesEmailExist);

}

/*
let name1, name2;
let charArr = ["x",".","/","2","j","M","a","r","t","i","n","J","m","M","i","g","u","e","l","f","e","y"];
getName();
function getName(){
	name1 = charArr.slice(5,11);
	name2 = charArr.slice(13,19);
	console.log(name1.join(''));
	console.log(name2.join(''));
}*/
/*
Mutator array methods
sort()
unshift()
shift()
splice()
push()
reverse()
pop()
fill()
forEach()

Non-mutator array methods
slice()
join()
includes()
filter()
concat()
every()
find()
findIndex()
map()
reduce()
some()
flat()
flatMap()

*/